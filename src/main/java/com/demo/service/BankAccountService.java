package com.demo.service;

import com.demo.domain.BankAccount;
import com.demo.repository.BankAccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class BankAccountService {

    @Autowired
    private BankAccountRepository bankAccountRepository;

    /**
     * find BankAccount by Id
     * @param id
     * @return
     */
    public Mono<BankAccount> findById(Long id) {
        return Mono.justOrEmpty(bankAccountRepository.findById(id));
    }

    public Mono<BankAccount> findByNumber(String number) {
        return Mono.justOrEmpty(bankAccountRepository.findByNumber(number));
    }

    /**
     * find List<BankAccount>
     * @return
     */
    public Flux<BankAccount> findAll() {
        return Flux.fromIterable(bankAccountRepository.findAll());
    }

    /**
     * save BankAccount
     * @param bankAccount
     * @return
     */
    @Transactional
    public Mono<BankAccount> add(Mono<BankAccount> bankAccount) {
        return Mono.justOrEmpty(bankAccountRepository.save(bankAccount.block()));
    }

    /**
     * update BankAccount
     * @return
     */
    @Transactional
    public Mono<BankAccount> update(Long id,Mono<BankAccount> bankAccount) {
        return Mono.justOrEmpty(bankAccountRepository.saveAndFlush(bankAccount.block()));
    }

}
