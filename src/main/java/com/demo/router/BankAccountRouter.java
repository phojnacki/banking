package com.demo.router;

import com.demo.banking.AddAccountRequest;
import com.demo.banking.BankingService;
import com.demo.banking.MoneyDepositWithdrawalRequest;
import com.demo.banking.MoneyTransferRequest;
import com.demo.domain.BankAccount;
import com.demo.service.BankAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.reactive.function.server.ServerResponse;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.web.reactive.function.server.RequestPredicates.GET;
import static org.springframework.web.reactive.function.server.RequestPredicates.POST;
import static org.springframework.web.reactive.function.server.RequestPredicates.accept;
import static org.springframework.web.reactive.function.server.RequestPredicates.path;
import static org.springframework.web.reactive.function.server.RouterFunctions.nest;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;
import static org.springframework.web.reactive.function.server.ServerResponse.ok;

import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.ServerRequest;

import reactor.core.publisher.Mono;

@Component
public class BankAccountRouter {

    @Autowired
    private BankAccountService bankAccountService;

    @Autowired
    private BankingService bankingService;

    public RouterFunction<ServerResponse> routes() {
        return nest(path("/bankAccount"),
                route(GET("/list").and(accept(APPLICATION_JSON)), this::listAll)
                        .andRoute(GET("/{number}").and(accept(APPLICATION_JSON)), this::findByNumber)
                        .andRoute(POST("/deposit").and(accept(APPLICATION_JSON)), this::moneyDeposit)
                        .andRoute(POST("/withdrawal").and(accept(APPLICATION_JSON)), this::moneyWithdrawal)
                        .andRoute(POST("/moneyTransfer").and(accept(APPLICATION_JSON)), this::moneyTransfer)
                        .andRoute(POST("/add").and(accept(APPLICATION_JSON)), this::createNewAccount));
    }

    private Mono<ServerResponse> findByNumber(ServerRequest req){
        String requestId = req.pathVariable("number");
        return ok().body(bankAccountService.findByNumber(requestId), BankAccount.class);
    }

    private Mono<ServerResponse> listAll(ServerRequest req) {
        return ok().body(bankAccountService.findAll(), BankAccount.class);
    }

    private Mono<ServerResponse> createNewAccount(ServerRequest req) {
        return ok().body(bankingService.createNewAccount(req.bodyToMono(AddAccountRequest.class)), BankAccount.class);
    }

    private Mono<ServerResponse> moneyWithdrawal(ServerRequest req) {
        return ok().body(bankingService.moneyWithdrawal(req.bodyToMono(MoneyDepositWithdrawalRequest.class)), BankAccount.class);
    }

    private Mono<ServerResponse> moneyDeposit(ServerRequest req) {
        return ok().body(bankingService.moneyDeposit(req.bodyToMono(MoneyDepositWithdrawalRequest.class)), BankAccount.class);
    }

    private Mono<ServerResponse> moneyTransfer(ServerRequest req) {
        return ok().body(bankingService.moneyTransfer(req.bodyToMono(MoneyTransferRequest.class)), BankAccount.class);
    }

}
