package com.demo.banking;

import lombok.Getter;
import lombok.Setter;

public class MoneyTransferRequest {

    @Getter @Setter private String accountNumberFrom;

    @Getter @Setter private String accountNumberTo;

    @Getter @Setter private Double amountOfMoney;

    public MoneyTransferRequest() { }

}
