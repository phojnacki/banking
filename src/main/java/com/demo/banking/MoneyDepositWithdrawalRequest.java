package com.demo.banking;

import lombok.Getter;
import lombok.Setter;

public class MoneyDepositWithdrawalRequest {

    @Getter @Setter private String accountNumber;

    @Getter @Setter private Double amountOfMoney;


    public MoneyDepositWithdrawalRequest() { }

}
