package com.demo.banking;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

public class AddAccountRequest {

    @Getter @Setter private String ownerName;

    @Getter @Setter private String ownerSurname;

    public AddAccountRequest() { }

}
