package com.demo.banking;

import com.demo.domain.BankAccount;
import com.demo.service.BankAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.util.UUID;

@Service
public class BankingService {

    @Autowired
    private BankAccountService bankAccountService;

    public Mono<BankAccount> createNewAccount(Mono<AddAccountRequest> addAccountRequestMono) {
        return addAccountRequestMono.flatMap(value -> {
            BankAccount bankAccount = new BankAccount();
            bankAccount.setOwnerName(value.getOwnerName());
            bankAccount.setOwnerSurname(value.getOwnerSurname());
            bankAccount.setNumber(UUID.randomUUID().toString());
            bankAccount.setBalance(0d);
            return bankAccountService.add(Mono.just(bankAccount));
        });
    }

    public Mono<BankAccount> moneyWithdrawal(Mono<MoneyDepositWithdrawalRequest> moneyWithdrawalMono) {
        return moneyWithdrawalMono.flatMap(value -> {
            return bankAccountService.findByNumber(value.getAccountNumber()).flatMap(account -> {
                account.setBalance(account.getBalance() - value.getAmountOfMoney());
                bankAccountService.update(account.getId(), Mono.just(account));
                return Mono.just(account);
            });
        });
    }

    public Mono<BankAccount> moneyDeposit(Mono<MoneyDepositWithdrawalRequest> moneyDepositMono) {
        return moneyDepositMono.flatMap(value -> {
            return bankAccountService.findByNumber(value.getAccountNumber()).flatMap(account -> {
                account.setBalance(account.getBalance() + value.getAmountOfMoney());
                bankAccountService.update(account.getId(), Mono.just(account));
                return Mono.just(account);
            });
        });
    }

    public Mono<BankAccount> moneyTransfer(Mono<MoneyTransferRequest> moneyTransferMono) {
        return moneyTransferMono.flatMap(value -> {
            return bankAccountService.findByNumber(value.getAccountNumberFrom()).flatMap(accountFrom -> {
                accountFrom.setBalance(accountFrom.getBalance() - value.getAmountOfMoney());
                bankAccountService.update(accountFrom.getId(), Mono.just(accountFrom));
                return Mono.just(accountFrom);
            }).then(bankAccountService.findByNumber(value.getAccountNumberTo()).flatMap(accountTo -> {
                accountTo.setBalance(accountTo.getBalance() + value.getAmountOfMoney());
                bankAccountService.update(accountTo.getId(), Mono.just(accountTo));
                return Mono.just(accountTo);
            }));
        });
    }


}
