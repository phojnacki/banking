package com.demo.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
public class BankAccount {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter @Setter private Long id;

    @Column(nullable = false)
    @Getter @Setter private String ownerName;

    @Column(nullable = false)
    @Getter @Setter private String ownerSurname;

    @Column(nullable = false)
    @Getter @Setter private String number;

    @Column(nullable = false)
    @Getter @Setter private Double balance;
//    @Column(nullable = false)
//    @Getter @Setter private List<Double> history;

    public BankAccount() { }

}
