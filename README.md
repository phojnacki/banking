# UBS demo

## Add account
````
POST http://127.0.0.1:9090/bankAccount/add
{
  "ownerName" : "Dmytro",
  "ownerSurname" : "Surname"
	
}
````
````
POST http://127.0.0.1:9090/bankAccount/add
{
  "ownerName" : "Karolina",
  "ownerSurname" : "Surname2"
}
````
## Deposit
````
POST http://127.0.0.1:9090/bankAccount/deposit
{
  "accountNumber" : "4d018dd2-7404-422e-8cd7-d22f97a8272d",
  "amountOfMoney" : "33"
}
````
## Withdrawal
````
POST http://127.0.0.1:9090/bankAccount/withdrawal
{
  "accountNumber" : "4d018dd2-7404-422e-8cd7-d22f97a8272d",
  "amountOfMoney" : "33"
}
````
## Money transfer
````
POST http://127.0.0.1:9090/bankAccount/moneyTransfer
{
  "accountNumberFrom" : "adf6929e-82d7-4fb0-9063-c54f72d7ce38",
  "accountNumberTo" : "33c5f23d-53b8-4177-a1bf-ac1564db9cab",
  "amountOfMoney" : "13"
}
````
## List helper
````
GET http://127.0.0.1:9090/bankAccount/list
````